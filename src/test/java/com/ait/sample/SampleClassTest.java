package com.ait.sample;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SampleClassTest {
	
	@Test
	void test() {
		assertEquals("Goodbye", SampleClass.getString());
	}
	@Test
	void test_Marks_GreaterThanEqualsTo_90() {
		assertEquals("A", SampleClass.getMarks(94));
	}
	@Test
	void test_Marks_GreaterThanEqualsTo_70() {
		assertEquals("B", SampleClass.getMarks(76));
	}
	@Test
	void test_Marks_GreaterThanEqualsTo_60() {
		assertEquals("C",SampleClass.getMarks(68));
	}
	@Test
	void test_Marks_GreaterThanEqualsTo_50() {
		assertEquals("D",SampleClass.getMarks(58));
	}
	@Test
	void test_Marks_GreaterThanEqualsTo_40() {
		assertEquals("E",SampleClass.getMarks(48));
	}
	@Test
	void test_Marks_LessThan_40() {
		assertEquals("FAIL",SampleClass.getMarks(28));
	}

}
